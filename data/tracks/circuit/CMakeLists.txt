INCLUDE(../../../cmake/macros.cmake)

# Official track sets.
SD_ADD_SUBDIRECTORY(espie)

# Work-in-progress track sets.
IF(NOT OPTION_OFFICIAL_ONLY)
	SD_ADD_SUBDIRECTORY(brno)
	SD_ADD_SUBDIRECTORY(bueno)
	SD_ADD_SUBDIRECTORY(dijon)
ENDIF()
